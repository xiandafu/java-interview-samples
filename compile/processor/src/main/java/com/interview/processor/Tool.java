package com.interview.processor;

import java.lang.annotation.*;

/**
 * 在编译期间为类提供一个私有构造函数，并且设置此类为final
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
@Documented
public @interface Tool {
}
