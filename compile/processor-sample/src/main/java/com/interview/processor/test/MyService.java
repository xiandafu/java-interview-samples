package com.interview.processor.test;

import com.interview.processor.RestCode;
import org.springframework.stereotype.Service;

@RestCode
@Service
public class MyService {
	public Integer add(int a){
		return a+1;
	}
	public Integer dec(int a){
		return a-1;
	}
	public Integer decdec(int a){
		return a-1-1;
	}
}
