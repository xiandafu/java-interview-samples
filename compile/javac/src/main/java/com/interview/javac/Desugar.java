package com.interview.javac;

import java.util.ArrayList;
import java.util.List;

/**
 * 用idea打开编译弟弟class文件
 */
public class Desugar {
	public void unbox(){
		Integer i = 1;
		Integer b = Integer.valueOf(1);
	}

	public void strSwtich(){
		String a = "1";
		switch(a){
			case "1":return;
		}
	}

	public void run() {
		List<String> list = new ArrayList();
		list.add("abc");
		String a = list.get(0);
	}
}
