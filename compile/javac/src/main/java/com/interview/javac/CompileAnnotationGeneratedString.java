package com.interview.javac;



import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static javax.tools.StandardLocation.*;

public class CompileAnnotationGeneratedString {

	public static void main(String[] args) throws IOException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

		DiagnosticCollector<JavaFileObject> diagnostics =
				new DiagnosticCollector<>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
		JavaStringObject stringObject =
				new JavaStringObject("MyService.java", getSource());

		String classes = System.getProperty("user.dir")+"/compile/javac/target/classes";
		File classesFile = new File(classes);


		fileManager.setLocation(CLASS_OUTPUT,Arrays.asList(classesFile));
		//指定生成代码输出的目录
		String src = System.getProperty("user.dir")+"/compile/javac/target/generated-sources/annotations";
		fileManager.setLocation(SOURCE_OUTPUT,Arrays.asList(new File(src)));


		JavaCompiler.CompilationTask task = compiler.getTask(null,
				fileManager, diagnostics, null, null, Arrays.asList(stringObject));
//		task.setProcessors(Arrays.asList(new RestCodeProcessor()));
		boolean success = task.call();
		System.out.println(success?"编译成功":"编译失败");
		diagnostics.getDiagnostics().forEach(System.out::println);

	}

	/**
	 * 编译后，Test变成final
	 * @return
	 */
	public static String getSource() {
		return "package spring;import com.interview.processor.RestCode; "
				+ "@RestCode\n"
				+ "public class MyService  {"
				+ "  public int doSomething(int v){"
				+ "     return v++;"
				+ "  }"
				+ "}";
	}
}

